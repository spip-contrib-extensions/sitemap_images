<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// S
	'sitemap_images_description' => 'Remplace le sitemap fourni par défaut dans SPIP par un sitemap plus complet comprenant les images du site',
	'sitemap_images_nom' => 'Sitemap images',
	'sitemap_images_slogan' => 'Sitemap comprenant les images',
);
